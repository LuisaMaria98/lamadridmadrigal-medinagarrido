print ("Realizar un programa que tenga una clase Persona con las siguientes características. La clase tendrá como atributos el nombre y la edad de una persona. Implementar los métodos necesarios para inicializar los atributos, mostrar los datos e indicar si la persona está en el rango de edad: Joven (14-26 años), Adulto (27-59 años), Persona Mayor (60 años en adelante). Validar edad mínima de 14 para clasificación.")
print(sep=" ")
class Persona:
  def __init__(self, nombre, edad):
    self.nombre= nombre
    self.edad= edad
    self.val = True  
  
  def validar (self):
    if self.edad >= 14:
      self.val = True
    else:
      self.val = False
    return self.val

  
  def clasificacion (self):
    if self.validar():
      if 14 <= self.edad <= 26:
        print(f"La persona {self.nombre} de edad {self.edad} es joven")
      elif 27 <= self.edad <= 59:
        print(f"La persona {self.nombre} de edad {self.edad} es adulto")
      else:
        print(f"La persona {self.nombre} de edad {self.edad} es mayor")
    else:
      print(f"La persona {self.nombre} de edad {self.edad} esta fuera del rango de clasificacion")

per1 = Persona ("Matilda", 61)
per2 = Persona("Matildo", 13)
per3 = Persona ("Mati", 18)
per4 = Persona ("Matt", 35)
per5 = Persona ("Lamadrid", 21)

per1.clasificacion ()
per2.clasificacion ()
per3.clasificacion ()
per4.clasificacion ()
per5.clasificacion ()